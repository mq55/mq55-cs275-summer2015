{\rtf1\ansi\ansicpg1252\cocoartf1347\cocoasubrtf570
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;\red0\green0\blue0;}
{\*\listtable{\list\listtemplateid1\listhybrid{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\*\levelmarker \{decimal\}.}{\leveltext\leveltemplateid1\'02\'00.;}{\levelnumbers\'01;}\fi-360\li720\lin720 }{\listname ;}\listid1}}
{\*\listoverridetable{\listoverride\listid1\listoverridecount0\ls1}}
\margl1440\margr1440\vieww18460\viewh12140\viewkind0
\deftab720
\pard\tx220\tx720\pardeftab720\li720\fi-720
\ls1\ilvl0
\f0\fs24 \cf2 \expnd0\expndtw0\kerning0
\outl0\strokewidth0 \strokec2 Mariana Quinde Garcia \
email: mq55@drexel.edu\
Brandon McKay\
email: bdm57@drexel.edu\
CS275 Web and Mobile App Development\
Lab 2\
Calendar Readers\
\
1. General Description:\
This lab consists of two programs that perform the same task using different tools. Both of them print the user\'92s list of Calendars, and a list of events for the first Calendar \
found. They use a project created in the Google Developer Console, called MarianaQuindeLab2, with a set ClientId for native application (for the first part of the lab) and a \
Client Id for web applications (for the Temboo part). \
\
The first part consists of a program called CalendarReader. This program conducts the OAuth protocol to obtain and validate clients credentials for every user of the \
application. It requires the user to authorize its access to their Google Calendars by accessing a url and clicking accept. The browser then provides a key to verify this \
authorization, which the user should copy in the console. The program uses this token to access the Google Calendars API, obtain the calendars data and print it.\
\
The second part consists of a program called CalendarReaderTemboo. As the name indicates, this program conducts the OAuth protocol and the calls to the Google \
Calendars API using Temboo\'92s chromeos. This process still requires the user to access a url and authorize access to their Google Calendars. However, the user needs not\
provide a key after that, only hit enter. The program then obtains a key through the OAuth chromeo and uses it with the Google Calendars chromeo, to obtain the calendars \
data and print it.\
\
2. Compiler / programming environment and operating system / computer that were used to complete your lab/assignment:\
Both programs were written using Eclipse in OS X Yosemite.\
\
3. Requirements to run your program \
Both programs need the GSON jar library (which is already in this directory) and should specify it in the class path as mentioned below. The second project also makes use of \
Temboo\'92s library (since it uses the InitializeOAuth, FinalizeOAuth and the Google Calendars Chromeos). The jar library is also included in this directory and needs to be\
specified in the classpath. Executable jar files of both programs are also included.\
\
To run CalendarReader:\
	\'93java -jar CalendarReader.jar"\
	or \
	\'93javac -classpath .:gson-2.2.4.jar CalendarReader.java\'94 and \'93java -classpath .:gson-2.2.4.jar CalendarReader\'94\
\
Once it runs, it will ask the user to visit a url to authorize use of Google Calendars and prompt the user for the key that the redirecting url provides.\
\
To run CalendarReaderTemboo:\
	\'93java -jar CalendarReaderTemboo.jar\'94\
	or\
	\'93javac -classpath .:gson-2.2.4.jar:temboo_java_sdk_2.11.1.jar CalendarReaderTemboo.java\'94 and \'93java -classpath .:gson-2.2.4.jar:temboo_java_sdk_2.11.1.jar CalendarReaderTemboo\'94\
Once it runs, it will ask the user to visit a url to authorize use of Google Calendars, the user can then hit enter or wait for the program to continue on its own.\
It is required that the user has a Google account. \
\
4. Limits of Functionality\
The programs will not work if the key provided is not valid, since the url with redirect to a page with an error and not allow the program to obtain access keys.\
The JSON parsing in the programs will work even if there are no calendars to display, or if there are no events. The data fields are handled assuming that calendars have \
summaries (names), but not assuming that they have descriptions (displays \'93Calendar\'94 if not found). The programs are also prepared to receive events with different times \
(prepared to handled normal and all day events), however it assumes that all events have a start parameter and a title. \
\
5. Testing\
Several tests where made using different calendars, events and accounts. The results were satisfying. The code is executed efficiently and the data is displayed as expected. \
There were issues when displaying all day events and calendars without description, since the parsing would crash. However, it was fixed by testing if such parameters \
existed during the parsing. The tests were performed using eclipse during the first stage, and running from the command line afterwards. There were no issues with the keys \
or with the Temboo chromeos.\
\
6. Special Facts\
Since the assignment does not specify if the events and calendars should refrain to upcoming elements, it was decided to display all events found. However, this is easily \
modifiable in the call to Googles API, a single extra parameter in the url will suffice, as well as the change in the Temboo chromeo. The task with Temboo was definitely \
easier than when executing all the steps of the OAuth process and the call to the API independently. However, it was a great learning experience to go through all the \
process before using Temboo and compare the code. Difficulties came up when coding the OAuth Protocol process, there were small errors in the url definition at first, but \
they were fixed afterwards. The API access was easier, in both projects.\
\
7. Improvement\
The links provided in the lab instructions are outdated, since new technologies or calls are being used by Google Developers. The documentation is very useful, but it was \
difficult to find the updated information.}