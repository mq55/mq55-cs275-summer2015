import java.io.*;
import java.net.*;
import java.util.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CalendarReader {
	private static Scanner scanner;

	/**
	 * Undergoes OAuth Authorization process, accesses user's calendar through Google Calendar API and prints data (without Temboo Libs)
	 * @param args
	 * @throws Exception 
	 */
	public static void main (String[] args) throws Exception {	
		// Project credentials settings
		String clientId = "253125127223-8dlvfj2e76bsvb1rb1mk5jqh7s2s07mv.apps.googleusercontent.com";
		String redirectUri = "urn:ietf:wg:oauth:2.0:oob";
		String clientSecret = "ByBp1y3ZNwyvrYLMQ0McH9K6";
		
		// URL to grant access
		String oauthUrl = "https://accounts.google.com/o/oauth2/auth?"
				+ "access_type=offline"
				+ "&client_id=" + clientId
				+ "&scope=https://www.googleapis.com/auth/calendar"
				+ "&response_type=code"
				+ "&redirect_uri=" + redirectUri 
				+ "&state=/profile"
				+ "&approval_prompt=force"; 
		
		// Prompt user for access code
		System.out.println("Please visit this link and click accept to give us access to your calendars. Enter the code provided when you are done:\n\n" + oauthUrl);
		
		scanner = new Scanner(System.in);
		String code = scanner.nextLine();
		
		// URL to get refresh token
		String authorizeURL = "https://accounts.google.com/o/oauth2/token" ;
		
		String authorizeParams = "code=" + code + 
				"&client_id=" + clientId + 
				"&client_secret=" + clientSecret +
				"&redirect_uri=" + redirectUri + 
				"&grant_type=authorization_code";

		// Get refresh token in JSON
		String authorizeResponse = executePost(authorizeURL,authorizeParams) ;
		
		// Parse token out
		JsonParser oauth_jp = new JsonParser();
		JsonElement oauth_root = oauth_jp.parse(authorizeResponse);
		JsonObject oauth_rootobj = oauth_root.getAsJsonObject(); 
		String refresh_token = oauth_rootobj.get("refresh_token").getAsString();
		
		// URL to get Access Token
		String tokenParams = "client_id=" + clientId + 
				"&client_secret=" + clientSecret +
				"&refresh_token=" + refresh_token + 
				"&grant_type=refresh_token";
		
		// Get Access Token in JSON 
		String tokenResponse = executePost(authorizeURL, tokenParams) ;
		
		// Parse token out
		oauth_jp = new JsonParser();
		oauth_root = oauth_jp.parse(tokenResponse);
		oauth_rootobj = oauth_root.getAsJsonObject(); 
		String access_token = oauth_rootobj.get("access_token").getAsString();
		
		// Read Calendar List
		String sURL = "https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token=" + access_token; 
		
		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection calendarRequest = (HttpURLConnection) url.openConnection();
		calendarRequest.connect();
		
		//Convert to a JSON object
		JsonParser calendarParser = new JsonParser();
		JsonElement calendarRoot = calendarParser.parse(new InputStreamReader((InputStream) calendarRequest.getContent()));
		JsonObject calendarRootObj = calendarRoot.getAsJsonObject();
				
		//Convert to a JSON array holding a calendar on each element
		JsonArray calendars = calendarRootObj.get("items").getAsJsonArray(); 
		
		// Calendar List
		System.out.println("\n\n--------------- List of Calendars: ---------------");
		String firstId = null;
		String firstName = null;
		
		// Loop through calendars
	    for (int i=0; i<calendars.size(); i++)
	    {
	    	// Convert calendar to JSON Object
	    	JsonObject element = calendars.get(i).getAsJsonObject();
	    	
	    	// Obtain calendar name and description
	    	String name = element.get("summary").getAsString();
	    	String description = "Calendar";
	    	
	    	if(element.has("description")){
	    		description = element.get("description").getAsString();
	    	}
	    	
	    	// Store id for first calendar
	    	if(i==0){
	    		firstId = element.get("id").getAsString();
	    		firstName = name;
	    	}
	    	
	    	// Print Calendar
	    	System.out.println(name);
	    	System.out.println("- " + description + "\n");
	    }
	    
	    // Display first calendar events (if exists)
	    if(firstId!= null){
	    	System.out.println("\n\n\n---------- Events of First Calendar (" + firstName + "): ----------");
	    	
	    	// Get Calendar events
	    	sURL = "https://www.googleapis.com/calendar/v3/calendars/"+ firstId + "/events?access_token=" + access_token; 
			
			// Connect to the URL
			url = new URL(sURL);
			HttpURLConnection eventsRequest = (HttpURLConnection) url.openConnection();
			eventsRequest.connect();
			
			//Convert to a JSON object
			JsonParser eventsParser = new JsonParser();
			JsonElement eventsRoot = eventsParser.parse(new InputStreamReader((InputStream) eventsRequest.getContent()));
			JsonObject eventsRootObj = eventsRoot.getAsJsonObject();
					
			//Convert to a JSON array holding an event on each element
			JsonArray events = eventsRootObj.get("items").getAsJsonArray(); 
			
			//Loop through events
		    for (int i=0; i<events.size(); i++)
		    {
		    	//Convert hour to JSON Object
		    	JsonObject element = events.get(i).getAsJsonObject();
		    	
		    	String title = element.get("summary").getAsString();
		    	String date;
		    	String time;
		    	
		    	// Get date and time
		    	JsonObject start = element.get("start").getAsJsonObject();
		    	if(start.has("dateTime")){
		    		// For normal events with time
		    		String dateTime = start.get("dateTime").getAsString();
		    		int indexT = dateTime.indexOf('T');
		    		date = dateTime.substring(0, indexT);
		    		time = dateTime.substring(indexT+1);
		    	}
		    	else
		    	{
		    		// For all day events
		    		date = start.get("date").getAsString();
		    		time = "All day event";
		    	}
		    	
		    	// Print data
		    	System.out.println("Title: " + title);
		    	System.out.println("- Date: " + date);
		    	System.out.println("- Time: " + time + "\n");
		    }
	    }
	}
	
	/**
	 * Executes a HTTP POST action and returns connection response
	 * @param String targetURL: domain of post action and String urlParameters: parameters of action
	 * @return String response of POST action
	 */
	public static String executePost(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection connection = null;  
        try {
        	// Create connection
        	url = new URL(targetURL);
        	connection = (HttpURLConnection)url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setRequestProperty("Content-Type", 
              "application/x-www-form-urlencoded");
            
        	connection.setRequestProperty("Content-Length", "" + 
        			Integer.toString(urlParameters.getBytes().length));
        	connection.setRequestProperty("Content-Language", "en-US");  
            
        	connection.setUseCaches (false);
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
         
        	// Send request
        	DataOutputStream wr = new DataOutputStream (
                     connection.getOutputStream ());
        	wr.writeBytes (urlParameters);
        	wr.flush ();
        	wr.close ();

        	// Read Response   
        	InputStream is = connection.getInputStream();
        	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        	String line;
        	StringBuffer response = new StringBuffer(); 
         
        	while((line = rd.readLine()) != null) {
        		response.append(line);
        		response.append('\r');
        	}
        	rd.close();
         
        	return response.toString();
        } catch (Exception e) {
        	
        	e.printStackTrace();
            return null;
        } finally {
        	if(connection != null) {
        		connection.disconnect(); 
        	}
        }
    }
}