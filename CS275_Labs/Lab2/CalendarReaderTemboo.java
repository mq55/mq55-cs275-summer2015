import java.util.Scanner;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.*;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Google.OAuth.InitializeOAuth.*;
import com.temboo.core.TembooSession;

public class CalendarReaderTemboo{
	private static Scanner scanner;
	
	/**
	 * Undergoes OAuth Authorization process, accesses user's calendar through Google Calendar API and prints data (using Temboo Libs)
	 * @param args
	 * @throws Exception 
	 */
	public static void main (String [] args) throws Exception{
		// Project credentials settings
		String clientId = "253125127223-k58ada2neuafjahsi6gmp998pen8up9v.apps.googleusercontent.com";
		String clientSecret = "QnfptwVF1-mATfnChU96XpF1";
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("mquinde", "CalendarLab2", "179acbb35ab94f248a6dfd918facf284");

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ClientID(clientId);
		initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar");

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		String callbackId = initializeOAuthResults.get_CallbackID();
		
		// Prompt user to give access through link
		System.out.println("Please visit this link and click accept to give us access to your calendars. Hit enter when you are done:\n" + initializeOAuthResults.get_AuthorizationURL());
		
		// Continue and finalize authorization
		scanner = new Scanner(System.in);
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callbackId);
		finalizeOAuthInputs.set_ClientSecret(clientSecret);
		finalizeOAuthInputs.set_ClientID(clientId);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		// Get refresh token
		String refresh_token = finalizeOAuthResults.get_AccessToken();
		
		// Read Calendars
		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		getAllCalendarsInputs.set_ClientSecret(clientSecret);
		getAllCalendarsInputs.set_RefreshToken(refresh_token);
		getAllCalendarsInputs.set_ClientID(clientId);

		// Execute Choreo
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
		
		// Convert to a JSON object
		JsonParser calendarParser = new JsonParser();
		JsonElement calendarRoot = calendarParser.parse(getAllCalendarsResults.get_Response());
		JsonObject calendarRootObj = calendarRoot.getAsJsonObject();
		
		
		// Convert to a JSON array holding a calendar on each element
		JsonArray calendars = calendarRootObj.get("items").getAsJsonArray(); 
		
		// Calendar List
		System.out.println("\n\n--------------- List of Calendars: ---------------");
		String firstId = null;
		String firstName = null;
				
		// Loop through calendars
		for (int i=0; i<calendars.size(); i++)
		{
			// Convert calendar to JSON Object
			JsonObject element = calendars.get(i).getAsJsonObject();
			    	
			// Get calendar name and description
			String name = element.get("summary").getAsString();
			String description = "Calendar";
			    	
			if(element.has("description")){
				description = element.get("description").getAsString();
			}
			    	
			// Store id of first calendar
			if(i==0){
				firstId = element.get("id").getAsString();
			    firstName = name;
			}
			    	
			// Print data
			System.out.println(name);
			System.out.println("- " + description + "\n");
		}
		
		// Display first calendar events (if exists)
	    if(firstId!= null){
	    	System.out.println("\n\n\n---------- Events of First Calendar (" + firstName + "): ----------");
	    	
	    	// Get calendar events
	    	GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

	    	// Get an InputSet object for the choreo
	    	GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

	    	// Set inputs
	    	getAllEventsInputs.set_ClientSecret(clientSecret);
	    	getAllEventsInputs.set_CalendarID(firstId);
	    	getAllEventsInputs.set_RefreshToken(refresh_token);
	    	getAllEventsInputs.set_ClientID(clientId);

	    	// Execute Choreo
	    	GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
	    	
	    	// Convert to a JSON object
			JsonParser eventsParser = new JsonParser();
			JsonElement eventsRoot = eventsParser.parse(getAllEventsResults.get_Response());
			JsonObject eventsRootObj = eventsRoot.getAsJsonObject();
					
			// Convert to a JSON array holding an event on each element
			JsonArray events = eventsRootObj.get("items").getAsJsonArray(); 
			
			// Loop through events
		    for (int i=0; i<events.size(); i++)
		    {
		    	// Convert event to JSON Object
		    	JsonObject element = events.get(i).getAsJsonObject();
		    	
		    	// Get title, date and time of event
		    	String title = element.get("summary").getAsString();
		    	String date;
		    	String time;
		    	
		    	// Get date and time
		    	JsonObject start = element.get("start").getAsJsonObject();
		    	if(start.has("dateTime")){
		    		// For normal events with time
		    		String dateTime = start.get("dateTime").getAsString();
		    		int indexT = dateTime.indexOf('T');
		    		date = dateTime.substring(0, indexT);
		    		time = dateTime.substring(indexT+1);
		    	}
		    	else
		    	{
		    		// For all day events
		    		date = start.get("date").getAsString();
		    		time = "All day event";
		    	}
		    	
		    	// Print data
		    	System.out.println("Title: " + title);
		    	System.out.println("- Date: " + date);
		    	System.out.println("- Time: " + time + "\n");
		    }
	    }
	}

}
