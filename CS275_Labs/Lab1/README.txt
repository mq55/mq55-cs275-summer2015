{\rtf1\ansi\ansicpg1252\cocoartf1347\cocoasubrtf570
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural

\f0\fs24 \cf0 Mariana Quinde Garcia \
email: mq55@drexel.edu\
Brandon McKay\
email: bdm57@drexel.edu\
CS275 Web and Mobile App Development\
Lab 1\
\
This lab consists of 2 java projects, which can be run using their respective scripts. The first one, LabPagePrinter, is a warmup exercise that displays the HTML content of the CS275 website for the current lab. The second one, WeatherLookUp.jar, displays an hourly forecast of the user\'92s current location using a Weather Underground client. Both programs were written using Eclipse in OS X Yosemite.\
\
To run LabPagePrinter:  \
	\'93java -jar LabPagePrinter.jar\'94 \
	or \
	\'93javac LabPagePrinter.java\'94 and \'93java LabPagePrinter\'94\
\
To run WeatherLookUp: \
	\'93java -jar WeatherLookUp.jar\'94\
	or\
	\'93javac -cp gson-2.2.4.jar:. WeatherLookUp.java\'94 and \'93java -cp gson-2.2.4.jar:. 	WeatherLookUp\'94\
\
None of the programs has command line parameters, they display the desired information automatically after being run. The programs were designed and implemented following the lab instructions. For WeatherLookUp, it was necessary first implement the geolookup, using Weather Underground\'92s feature. After that, the hourly forecast information for the users location is queried from Weather Underground and displayed. \
\
Both programs were tested simply by running them several times. LabPagePrinter.jar succeeded all the tests, showing the html for the lab page every time. WeatherLookUp.jar also succeeded all the tests: it was run at 5:00pm on Thursday, July 2nd, several times at 4:00pm and 6:00pm on Monday, July 6th, always showing the hourly forecast provided by Weather Underground.\
\
The lab was easy so complete by following the instructions on the website and with what we learned in lectures. We decided to create a jar file for both programs in order to run them faster, however, it is also possible to run the java files using the commands mentioned above. \
\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\pardirnatural
\cf0 \
EXTRA CREDIT: We added the autocomplete widget to lookup cities using the Weather Underground feature. Used lab instructions. Redirected to the San Francisco weather underground page (we did not have time to add redirection to looked up city). \
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural
\cf0 \
\
}