import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.*;

public class LabPagePrinter{         
	/**
	 * Prints html for lab 1
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		//Lab page url
		String sURL = "https://www.cs.drexel.edu/~augenbdh/cs275_su15/labs/wxunderground.html";
		
		//Connect to the URL
		URL url = new URL(sURL);
		URLConnection connection = url.openConnection();
		
		//Read URL
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    connection.getInputStream()));
        //Print line by line of page
        String inputLine;
        while ((inputLine = in.readLine()) != null) 
            System.out.println(inputLine);
        
        //Close reader
        in.close();
	}
}