import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class WeatherLookUp {
	/**
	 * Looks up user current location and displays hourly weather forecast
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// Weather Ground URL with geolookup
		String sURL = "http://api.wunderground.com/api/79a5c2c4d5c9d8fa/geolookup/q/autoip.json";
		
		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
			      
		//Convert to a JSON object to print data
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
		    	
		//Get zip code, city and state
    	String zipcode = rootobj.get("location").getAsJsonObject().get("zip").getAsString();
		String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
		String state = rootobj.get("location").getAsJsonObject().get("state").getAsString();
		
		//Print location
		System.out.println("Location: " + city + ", " + state + " (" + zipcode + ")");
	    
		//String with hourly forecast for current location
		String hrURL = "http://api.wunderground.com/api/79a5c2c4d5c9d8fa/hourly/q/" + state + "/" + city + "/" + ".json";
		
		// Connect to the URL
		URL hrUrl = new URL(hrURL);
		HttpURLConnection hourlyRequest = (HttpURLConnection) hrUrl.openConnection();
		hourlyRequest.connect();
			      
		//Convert to a JSON object
		JsonParser hourlyParser = new JsonParser();
		JsonElement hourlyRoot = hourlyParser.parse(new InputStreamReader((InputStream) hourlyRequest.getContent()));
		JsonObject hourlyRootObj = hourlyRoot.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
		
		//Convert to a JSON array holding weather forecast for each hour on each element
		JsonArray a = hourlyRootObj.get("hourly_forecast").getAsJsonArray(); 
	    
		//Loop through hours
	    for (int i=0; i<a.size(); i++)
	    {
	    	//Convert hour to JSON Object
	    	JsonObject element = a.get(i).getAsJsonObject();
	    	
	    	//Get date, condition, temperature and humidity
	    	String hourDate = element.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
	    	String condition = element.get("condition").getAsString();
	    	String temp = element.get("temp").getAsJsonObject().get("english").getAsString();
	    	String rHumidity = element.get("humidity").getAsString();
	    	
	    	//Print forecast
	    	System.out.println(String.format("%0$30s", hourDate) + " Condition: " + String.format("%0$-15s",condition) + " Temperature: " + String.format("%0$3s",temp) + "°F" + " Humidity: " + String.format("%0$3s",rHumidity) + "%");
	    }
	}
	
}
