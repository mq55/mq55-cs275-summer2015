package com.example.mquinde.tictactoe;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Console;


public class PlayActivity extends ActionBarActivity {

    public String turn = "x";
    public int [] board = new int[9];
    public int count =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resetBoard();
        setContentView(R.layout.activity_play);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void resetBoard(){
        for(int i = 0; i<9; i++){
            board[i]=100;
        }
    }

    public void OnGridButtonClick(View v) {
        count++;
        Button b = (Button) v;
        String strId = ((String)b.getTag());
        int tag = Integer.parseInt(strId);
        if (turn.equals("x")){
            b.setText(R.string.X);
            board[tag]=1;
            turn = "y";
        }else {
            b.setText(R.string.Y);
            board[tag]=0;
            turn = "x";
        }
        b.setClickable(false);
        checkWin(tag);
    }

    public void OnNewGameButtonClick(View v) {
        int id = R.id.button1;
        for(int i = 0; i<9; i++){
            Button b = (Button) findViewById(id+i);
            b.setText(" ");
            b.setClickable(true);
        }
        turn = "x";
        resetBoard();
        count=0;
        TextView results = (TextView) findViewById(R.id.results);
        results.setText("Play!");
        results.setTextColor(Color.parseColor("#ffffff"));
        results.setBackgroundColor(Color.parseColor("#A1E3E2"));
    }

    public void stopGame() {
        int id = R.id.button1;
        for(int i = 0; i<9; i++){
            Button b = (Button) findViewById(id+i);
            b.setClickable(false);
        }
    }

    public void checkWin(Integer tag){
        int mod = tag%3;
        int vertSum = 300;
        int horSum = 300;
        int diagSumLeft = 300;
        int diagSumRight = 300;
        switch (mod) {
            case 0:
                vertSum = board[0]+board[3]+board[6];
                horSum = board[tag]+board[tag+1]+board[tag+2];
                if((tag==0)|(tag==6)){
                    diagSumRight = board[0]+board[4]+board[8];
                    diagSumLeft = board[6]+board[4]+board[2];
                }
                break;
            case 1:
                vertSum = board[1]+board[4]+board[7];
                horSum = board[tag-1]+board[tag]+board[tag+1];
                if(tag==4){
                    diagSumRight = board[0]+board[4]+board[8];
                    diagSumLeft = board[6]+board[4]+board[2];
                }
                break;
            case 2:
                vertSum = board[2]+board[5]+board[8];
                horSum = board[tag-2]+board[tag-1]+board[tag];
                if((tag==2)|(tag==8)){
                    diagSumRight = board[0]+board[4]+board[8];
                    diagSumLeft = board[6]+board[4]+board[2];
                }
                break;
        }

        if((vertSum==0)|(horSum==0)|(diagSumLeft==0)|(diagSumRight==0)|(vertSum==3)|(horSum==3)|(diagSumLeft==3)|(diagSumRight==3)){
            TextView results = (TextView) findViewById(R.id.results);
            if(vertSum==0){
                results.setText(R.string.oWin);
            }
            else {
                results.setText(R.string.xWin);
            }
            results.setTextColor(Color.parseColor("#BC0903"));
            results.setBackgroundColor(Color.parseColor("#BCBABA"));
            stopGame();

        } else if(count==9){
            TextView results = (TextView) findViewById(R.id.results);
            results.setText(R.string.tie);
            results.setTextColor(Color.parseColor("#BC0903"));
            results.setBackgroundColor(Color.parseColor("#BCBABA"));
            stopGame();
        }
    }
}
