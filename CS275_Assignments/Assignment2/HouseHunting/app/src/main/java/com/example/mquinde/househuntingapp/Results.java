package com.example.mquinde.househuntingapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;


public class Results extends ActionBarActivity {
    /**
     * Mariana Quinde Garcia
     * mq55
     * 08/23/2015
     * Results activity to display most efficient route
     */
    private ListView resultsList;
    private ResultsListAdapter resultsListAdapter;
    public static Route optimalRoute = new Route();
    public static OpenHouse selectedFrom;
    public static OpenHouse selectedTo;

    // Called when activity is created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        // get Text view for results
        TextView results = (TextView) findViewById(R.id.resultsTextView);

        // Get optimal route from main activity
        if(MainActivity.optimalRouteFound){
            optimalRoute = MainActivity.optimalRoute;
            results.setText("Optimal route found! Total Distance: " + optimalRoute.TotalDistance + " meters. (Click on each step to get directions)");
        }
        else {
            results.setText("No route can be created for current distance and schedule.");
        }

        // Initialize List for results
        resultsList = (ListView) findViewById(R.id.resultsList);
        resultsList.setItemsCanFocus(true);
        resultsList.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);

        // Initialize adapter and set it for list of results
        resultsListAdapter = new ResultsListAdapter(this);
        resultsList.setAdapter(resultsListAdapter);

        // Set the click listener for the list's items
        resultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // Go to Route activity for selected pair of houses
                List<OpenHouse> houses = resultsListAdapter.getOpenHouses(arg2);
                selectedFrom = houses.get(0);
                selectedTo = houses.get(1);
                goToRoute();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);

        // Initialize List for results
        resultsList = (ListView) findViewById(R.id.resultsList);
        resultsList.setItemsCanFocus(true);
        resultsList.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);

        // Initialize adapter and set it for list of results
        resultsListAdapter = new ResultsListAdapter(this);
        resultsList.setAdapter(resultsListAdapter);

        // Set the click listener for the list's items
        resultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // Go to Route activity for selected pair of houses
                List<OpenHouse> houses = resultsListAdapter.getOpenHouses(arg2);
                selectedFrom = houses.get(0);
                selectedTo = houses.get(1);
                goToRoute();
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToRoute(){
        // Go to activity
        try {
            Intent intent = new Intent(this, RouteActivity.class);
            this.startActivity(intent);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
