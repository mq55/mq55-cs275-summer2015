package com.example.mquinde.househuntingapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class RouteActivity extends ActionBarActivity {
    /**
     * Mariana Quinde Garcia
     * mq55
     * 08/23/2015
     * Route activity to display directions between two houses
     */

    public static OpenHouse fromHouse;
    public static OpenHouse toHouse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        // get houses selected and text view to show directions
        fromHouse = Results.selectedFrom;
        toHouse = Results.selectedTo;
        TextView DirectionsTextView = (TextView) findViewById(R.id.directionsTextView);

        // Execute async task to get directions from google
        try {
            new DirectionsGetter(DirectionsTextView,fromHouse,toHouse).execute();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_route, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // On click for see in google maps
    public void onClickRedirectToGoogle(View v){
        // Redirect to url
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(getUrl()));
        startActivity(intent);
    }

    // returns url for google maps
    public String getUrl(){
        String url = "https://www.google.com/maps/dir/";
        String from = fromHouse.Address.replace(" ","+").replace(",","+");
        String to = toHouse.Address.replace(" ","+").replace(",","+");
        url = url + from + "/" + to;
        return url;
    }

}
