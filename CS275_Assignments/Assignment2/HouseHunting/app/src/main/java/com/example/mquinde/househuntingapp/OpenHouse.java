package com.example.mquinde.househuntingapp;

import java.sql.Time;

/**
 * Mariana Quinde Garcia
 * mq55
 * 08/23/2015
 * Open house object
 */
public class OpenHouse {
    String Address;
    String StartTime;
    String EndTime;

    // Time in seconds since 00:00
    int Start;
    int End;

    public OpenHouse(){
        Address = "";
        StartTime = "";
        EndTime = "";
        Start = 0;
        End = 0;
    }

    public void setStart(String Time){
        StartTime = Time;
        String hours = Time.substring(0,Time.indexOf(":"));
        String minutes = Time.substring(Time.indexOf(":") + 1);
        Start = (Integer.parseInt(hours)*3600) + (Integer.parseInt(minutes)*60);
    }

    public void setEnd(String Time){
        EndTime = Time;
        String hours = Time.substring(0,Time.indexOf(":"));
        String minutes = Time.substring(Time.indexOf(":") + 1);
        End = (Integer.parseInt(hours)*3600) + (Integer.parseInt(minutes)*60);
    }
}
