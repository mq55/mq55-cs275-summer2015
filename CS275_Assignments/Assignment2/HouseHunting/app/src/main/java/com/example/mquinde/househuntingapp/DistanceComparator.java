package com.example.mquinde.househuntingapp;

import java.util.Comparator;

/**
 * Mariana Quinde Garcia
 * mq55
 * 08/23/2015
 * Compares routes based on distance
 */
public class DistanceComparator implements Comparator<Route> {
    @Override
    public int compare(Route r1, Route r2) {
        if(r1.TotalDistance<r2.TotalDistance){
            return -1;
        }
        if (r1.TotalDistance>r2.TotalDistance){
            return 1;
        }
        return 0;
    }
}
