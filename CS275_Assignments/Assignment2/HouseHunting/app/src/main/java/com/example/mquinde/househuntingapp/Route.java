package com.example.mquinde.househuntingapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Mariana Quinde Garcia
 * mq55
 * 08/23/2015
 * Route object
 */
public class Route {

    public List<OpenHouse> Houses;
    public double TotalDistance;
    public List<Integer> MapToOriginalIndexes;

    public Route(){
        Houses =  new ArrayList<OpenHouse>();
        TotalDistance = 0;
    }
}
