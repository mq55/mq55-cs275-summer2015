package com.example.mquinde.househuntingapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Layout;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Directions.GetDrivingDirections;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.util.ArrayList;
import java.util.List;


/**
 * Mariana Quinde Garcia
 * mq55
 * 08/24/2015
 * Gets directions from google using Temboo
 */
public class DirectionsGetter extends AsyncTask<Void, Void, String> {

    private TextView directionsTextView;
    private List<String> Directions = new ArrayList<>();
    private OpenHouse fromHouse;
    private OpenHouse toHouse;

    public DirectionsGetter(TextView textView, OpenHouse from, OpenHouse to){
        fromHouse = from;
        toHouse = to;
        this.directionsTextView = textView;
    }

    @Override
    protected String doInBackground(Void... arg0) {
        try {
            TembooSession session = null;
            try {
                session = new TembooSession("mquinde", "HouseHuntingApp", "ROnfhdlYX2YxrqJOf0fPGi1SvhMnXYfJ");
            } catch (TembooException e) {
                e.printStackTrace();
            }

            // Code obtained from Temboo.com Google Driving Choreo
            GetDrivingDirections getDrivingDirectionsChoreo = new GetDrivingDirections(session);

            // Get an InputSet object for the choreo
            GetDrivingDirections.GetDrivingDirectionsInputSet getDrivingDirectionsInputs = getDrivingDirectionsChoreo.newInputSet();

            // Set inputs
            getDrivingDirectionsInputs.set_Origin(fromHouse.Address);
            getDrivingDirectionsInputs.set_Destination(toHouse.Address);
            // Execute Choreo
            GetDrivingDirections.GetDrivingDirectionsResultSet getDrivingDirectionsResults = getDrivingDirectionsChoreo.execute(getDrivingDirectionsInputs);
            //End of Temboo code

            // Convert to a JSON object and find needed elements
            JsonParser routeParser = new JsonParser();
            JsonElement routeRoot = routeParser.parse(getDrivingDirectionsResults.get_Response());
            JsonObject routeRootObj = routeRoot.getAsJsonObject();
            JsonObject routes = (routeRootObj.get("routes").getAsJsonArray()).get(0).getAsJsonObject();
            JsonObject legs = (routes.get("legs").getAsJsonArray()).get(0).getAsJsonObject();
            JsonArray steps = legs.get("steps").getAsJsonArray();

            // Get every direction, format and add to list
            for (int i = 0; i< steps.size(); i++){
                JsonObject step = steps.get(i).getAsJsonObject();
                String direction = step.get("html_instructions").getAsString().replace("<div","\n<div").replace("\\u003cb\\u003e","").replaceAll("\\<.*?\\>", "");
                Directions.add(direction);
            }

        } catch(Exception e) {
            // if an exception occurred, log it
            Log.e(this.getClass().toString(), e.getMessage());
        }


        return null;
    }

    protected void onPostExecute(String result) {
        try {
            // Update UI
            String directionsString = "";
            for (int i = 0; i<Directions.size(); i++){
                directionsString = directionsString +String.valueOf(i+1) + ". " + Directions.get(i) + "\n\n";
            }
            directionsTextView.setText(directionsString);
        } catch(Exception e) {
            // show an error message
            Log.e(this.getClass().toString(), e.getMessage());
        }

    }
}
