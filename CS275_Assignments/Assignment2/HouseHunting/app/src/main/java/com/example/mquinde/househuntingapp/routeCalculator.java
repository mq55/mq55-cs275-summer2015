package com.example.mquinde.househuntingapp;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Directions.GetDrivingDirections;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;

/**
 * Created by mquinde on 8/23/15.
 */
public class routeCalculator extends AsyncTask<Void, Void, String> {
    /**
     * Mariana Quinde Garcia
     * mq55
     * 08/22/2015
     * Calculates all possible routes given the saves open houses and picks the most efficient one
     */

    private static final double halfHour = 1800;
    public List<OpenHouse> openHouses;
    public double[][] DistanceCosts;
    public double[][] DurationCosts;
    public List<Route> Routes;
    public Route mostEfficientRoute;
    public boolean foundRoute;
    private Activity activity;

    // Constructor
    public routeCalculator(List<OpenHouse> openHousesList, Activity activity){
        // Initializes all values
        this.activity = activity;
        openHouses = openHousesList;
        Routes = new ArrayList<>();
        DistanceCosts = new double[openHousesList.size()][openHousesList.size()];
        DurationCosts = new double[openHousesList.size()][openHousesList.size()];
    }

    @Override
    protected String doInBackground(Void... arg0) {
        // Get costs of travelling between all n houses to all other n houses
        getCosts();

        // Get route permutations
        List<Integer> indexes = new ArrayList<Integer>();
        routePermutation(indexes);

        // Sort by total route distance
        Collections.sort(Routes, new DistanceComparator());

        // Find the fist route that is possible according to times
        int validRoute = findValidRoute();

        // Set valid route in main activity if found
        if (validRoute != -1) {
            foundRoute = true;
            mostEfficientRoute = Routes.get(validRoute);
            MainActivity.setOptimalRoute(mostEfficientRoute);
        } else {
            foundRoute = false;
        }

        return null;
    }

    protected void onPostExecute(String result) {
        try {
            // start a results activity
            Intent intent = new Intent(activity, Results.class);
            activity.startActivity(intent);

        } catch(Exception e) {
            // show an error message
            Log.e(this.getClass().toString(), e.getMessage());
        }
    }

    // gets costs of all possible routes
    public void getCosts(){
        try {
            TembooSession session = null;
            try {
                session = new TembooSession("mquinde", "HouseHuntingApp", "ROnfhdlYX2YxrqJOf0fPGi1SvhMnXYfJ");
            } catch (TembooException e) {
                e.printStackTrace();
            }

            for (int i = 0; i<openHouses.size(); i++) {
                for (int j = 0; j < openHouses.size(); j++) {
                    if (i == j) {
                        DistanceCosts[i][j] = 0;
                        DurationCosts[i][j] = 0;
                    } else {

                        // Code obtained from Temboo.com Google Driving Choreo
                        GetDrivingDirections getDrivingDirectionsChoreo = new GetDrivingDirections(session);

                        // Get an InputSet object for the choreo
                        GetDrivingDirections.GetDrivingDirectionsInputSet getDrivingDirectionsInputs = getDrivingDirectionsChoreo.newInputSet();

                        // Set inputs
                        getDrivingDirectionsInputs.set_Origin(openHouses.get(i).Address);
                        getDrivingDirectionsInputs.set_Destination(openHouses.get(j).Address);

                        // Execute Choreo
                        GetDrivingDirections.GetDrivingDirectionsResultSet getDrivingDirectionsResults = getDrivingDirectionsChoreo.execute(getDrivingDirectionsInputs);
                        //End of Temboo code

                        // Convert to a JSON object and find needed elements
                        JsonParser routeParser = new JsonParser();
                        JsonElement routeRoot = routeParser.parse(getDrivingDirectionsResults.get_Response());
                        JsonObject routeRootObj = routeRoot.getAsJsonObject();
                        JsonObject routes = (routeRootObj.get("routes").getAsJsonArray()).get(0).getAsJsonObject();
                        JsonObject legs = (routes.get("legs").getAsJsonArray()).get(0).getAsJsonObject();

                        // Get distance in meters
                        JsonObject distance = legs.get("distance").getAsJsonObject();
                        String valueDistance = distance.get("value").getAsString();
                        DistanceCosts[i][j] = Double.parseDouble(valueDistance);

                        // Get Time in seconds
                        JsonObject duration = legs.get("duration").getAsJsonObject();
                        String valueDuration = duration.get("value").getAsString();
                        DurationCosts[i][j] = Double.parseDouble(valueDuration);
                    }
                }
            }
        } catch(Exception e) {
            // log exception
            Log.e(this.getClass().toString(), e.getMessage());
        }
    }

    // gets all possible routes (permutations) (Recursive function)
    public void routePermutation(List<Integer> houseIndexes){
        // Receives a list of integers and adds all possible indexes of houses to it
        for (int i = 0; i<openHouses.size(); i++){
            if(!houseIndexes.contains(i)){
                // Add possible indexes
                List<Integer> Indexes = new ArrayList<>();
                for (int j = 0; j < houseIndexes.size(); j++){
                    Indexes.add(houseIndexes.get(j));
                }

                Indexes.add(i);

                // If list is complete, create route and save
                if (Indexes.size() == openHouses.size()){
                    Route route = makeRoute(Indexes);
                    Routes.add(route);
                }
                else{
                    routePermutation(Indexes);
                }
            }
        }
    }

    // Makes a route object out of a list of indexes
    public Route makeRoute(List<Integer> indexes){
        // Init new route
        Route route = new Route();

        // Add open houses according to indexes list
        for (int index = 0; index < indexes.size(); index++){
            route.Houses.add(openHouses.get(indexes.get(index)));
        }

        // Add total distance
        route.TotalDistance = calculateTotalDistance(indexes);

        // Store sequence of indexes
        route.MapToOriginalIndexes = indexes;

        return route;
    }

    public double calculateTotalDistance(List<Integer> indexes){

        double distance = 0;

        // Add all distances
        for (int i = 0; i < indexes.size()-1; i++){
            distance = distance + DistanceCosts[indexes.get(i)][indexes.get(i+1)];
        }

        return distance;
    }

    public int findValidRoute(){
        int i = 0;

        // Go through all routes until we find one that is valid
        while(i<Routes.size()){
            boolean isValid = true;
            Route currentRoute = Routes.get(i);
            int j = 0;
            // Go through all houses in route
            while ((j<(currentRoute.Houses.size()-1)) & isValid){
                double endTimeOfLastOpenHouse = currentRoute.Houses.get(j).Start + halfHour;
                int lastHouseIndex = currentRoute.MapToOriginalIndexes.get(j);
                int nextHouseIndex = currentRoute.MapToOriginalIndexes.get(j+1);

                double trayectory = DurationCosts[lastHouseIndex][nextHouseIndex];
                double startOfNextHouse = openHouses.get(nextHouseIndex).Start;

                // Check if trayectory from one house to next is valid according to start and end times
                if((endTimeOfLastOpenHouse + trayectory)>startOfNextHouse){
                    isValid = false;
                }
                j++;
            }
            if (isValid){
                // return route (this is the most efficient and possible route)
                return i;
            }
            i++;
        }
        return (-1);
    }

}
