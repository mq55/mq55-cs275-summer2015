package com.example.mquinde.househuntingapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Mariana Quinde Garcia
 * mq55
 * 08/22/2015
 * Adapter for results list
 */
public class ResultsListAdapter extends BaseAdapter {

    Route optimalRoute = Results.optimalRoute;
    private Activity context;

    static class RouteView {
        protected TextView number;
        protected TextView from;
        protected TextView to;
    }

    public ResultsListAdapter(Activity activity) {
        context = activity;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (optimalRoute.Houses.size() - 1);
    }

    @Override
    public OpenHouse getItem(int arg0) {
        return optimalRoute.Houses.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {

        RouteView holder;
        if (arg1 == null) {
            holder = new RouteView();
            LayoutInflater inflator = context.getLayoutInflater();
            arg1 = inflator.inflate(R.layout.resultslistitem, null);
            holder.number = (TextView) arg1.findViewById(R.id.ResultNumber);
            holder.from = (TextView) arg1.findViewById(R.id.fromText);
            holder.to = (TextView) arg1.findViewById(R.id.StartView);
            arg1.setTag(holder);

        } else {
            holder = (RouteView) arg1.getTag();
        }

        OpenHouse fromHouse = optimalRoute.Houses.get(arg0);
        OpenHouse toHouse = optimalRoute.Houses.get(arg0 + 1);

        holder.number.setText(Integer.toString(arg0+1));
        holder.from.setText(fromHouse.Address);
        holder.to.setText(toHouse.Address);

        return arg1;
    }

    public List<OpenHouse> getOpenHouses(int position)
    {
        List<OpenHouse> houses = new ArrayList<>();
        houses.add(optimalRoute.Houses.get(position));
        houses.add(optimalRoute.Houses.get(position+1));
        return houses;
    }
}