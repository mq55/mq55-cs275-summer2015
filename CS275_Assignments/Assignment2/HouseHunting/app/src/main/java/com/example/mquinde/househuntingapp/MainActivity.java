package com.example.mquinde.househuntingapp;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.TextKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class MainActivity extends Activity {
    /**
     * Mariana Quinde Garcia
     * mq55
     * 08/22/2015
     * Main activity to allow users to enter input (open houses info)
     */

    private ListView inputList;
    private InputListAdapter inputListAdapter;
    private static List<OpenHouse> openHousesSavedList = new ArrayList<OpenHouse>();
    public static Route optimalRoute;
    public static boolean optimalRouteFound = false;

    // Called when Activity is created
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize List for results
        inputList = (ListView) findViewById(R.id.inputListView);

        // Initialize adapter and set it for list of results
        inputListAdapter = new InputListAdapter(this);
        inputList.setAdapter(inputListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        inputList = (ListView) findViewById(R.id.inputListView);

        // Initialize List for results
        inputList = (ListView) findViewById(R.id.resultsList);

        // Initialize adapter and set it for list of results
        inputListAdapter = new InputListAdapter(this);
        inputList.setAdapter(inputListAdapter);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Returns the current saves list of open houses
    public static List<OpenHouse> getOpenHousesSavedList() {
        // if list is empty initialize with two default open house
        return openHousesSavedList;
    }

    // Adds another item to the list so that the user ca
    public void addOpenHouse(View v){

        // add house to current list
        OpenHouse newOpenHouse = new OpenHouse();
        EditText address = (EditText) findViewById(R.id.AddressField);
        newOpenHouse.Address = address.getText().toString();
        EditText start = (EditText) findViewById(R.id.StartField);
        newOpenHouse.setStart(start.getText().toString());
        EditText end = (EditText) findViewById(R.id.EndField);
        newOpenHouse.setEnd(end.getText().toString());
        openHousesSavedList.add(newOpenHouse);

        inputListAdapter.notifyDataSetChanged();
        if (openHousesSavedList.size()>1){
            Button submit = (Button) findViewById(R.id.SubmitButton);
            submit.setEnabled(true);
        }
    }

    // Submits list as is right then and calculates optimal route
    public void submitList(View v){
        // Start async task of calculating route
        try {
            new routeCalculator(openHousesSavedList, this).execute();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    // Used by async tasks to set optimal route
    public static void setOptimalRoute(Route route){
        optimalRoute = route;
        optimalRouteFound = true;
    }

}