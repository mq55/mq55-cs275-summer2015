package com.example.mquinde.househuntingapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Mariana Quinde Garcia
 * mq55
 * 08/22/2015
 * Adapter for input list
 */
public class InputListAdapter extends BaseAdapter {

    List<OpenHouse> openHouses = MainActivity.getOpenHousesSavedList();
    private Activity context;

    static class houseView {
        protected TextView address;
        protected TextView start;
        protected TextView end;
    }

    public InputListAdapter(Activity activity) {
        context = activity;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (openHouses.size());
    }

    @Override
    public OpenHouse getItem(int arg0) {
        return openHouses.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {

        houseView holder;
        if (arg1 == null) {
            holder = new houseView();
            LayoutInflater inflator = context.getLayoutInflater();
            arg1 = inflator.inflate(R.layout.inputlistitem, null);
            holder.address = (TextView) arg1.findViewById(R.id.addressView);
            holder.start = (TextView) arg1.findViewById(R.id.startView);
            holder.end = (TextView) arg1.findViewById(R.id.endView);
            arg1.setTag(holder);

        } else {
            holder = (houseView) arg1.getTag();
        }

        OpenHouse house = openHouses.get(arg0);

        holder.address.setText(house.Address);
        holder.start.setText(house.StartTime);
        holder.end.setText(house.EndTime);

        return arg1;
    }
}
