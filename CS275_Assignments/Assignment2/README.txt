Mariana Quinde Garcia 
email: mq55@drexel.edu
CS275 Web and Mobile App Development
Assignment 2

SCREENCAST URL's: 
Part 1:
https://www.youtube.com/watch?v=37RfrQWT8fc&feature=youtu.be
Part 2:
https://www.youtube.com/watch?v=wpgwZfH1Dnk&feature=youtu.be

1. General Description:
This assignment consists of an android app that calculates the most distance
efficient route between open house events, according to their scheduled
hours. The app allows the user to input the address, start and end time of
as many open houses as they wish, calculates the optimal route and displays
directions to it, both in text and through Google maps. The app uses Temboo
libraries and the GSON library to obtain distance, duration and directions
data from Google Maps API. 

In the first view the user receives a view with fields to enter open house
information. A list view is updated to display the current information.
There are also two buttons, one to add another open house element and one to
submit elements. A Custom List Adaptor was used to display this.

Once the addresses are gathered, the app runs an async task in which it
obtains the most efficient route, if it exists. First it calculates the
distance and duration costs of all the trajectories between the houses using
Temboo libraries and stores the values in two two-dimensional arrays. Then
it creates all possible routes or permutations through a recursive function
that I created, which creates sequences of possible indexes and creates
Route objects with them.  The distance for each route is calculated and
then, all the possible routes are sorted according to their total distance.
Finally, each element in the Routes list is evaluated to see if it is
possible to perform this route given the time restrictions. The first route
to be valid is then the most efficient one, and another view in launched to
display the results.

The new view contains a list view with the steps (house a to house b) of the
optimal route. The user may click on each item and get directions from a to
b. The directions are obtained through an async task using temboo libraries
and then displayed in another view, which also contains a button that
redirects to google maps with directions of the selected route.

2. Compiler / programming environment and operating system / computer that
were used to complete your lab/assignment:
The program was written using Android Studio in OS X Yosemite.

3. Requirements to run your program 
The program needs Temboo’s library (since it uses the GetDirections choreo
for Google), and the GSON library (to get directions information from
Google’s API response). The jar libraries are included in the android studio
directory.
To run the application, run the emulator and click on the HouseHunterApp
icon.

4. Limits of Functionality
The program is rather slow because all the distance and duration costs have
to be calculated through calls to Temboo. The permutations also take time to
obtain since a recursive function is used and n! routes have to be made. The
directions of the open houses have to be displayed with a certain format in
order for the app to work (either as “2930 Chestnut St, Philadelphia, PA” or
“latitude, longitude”)

5. Testing and Special Facts
Several tests were using different addresses and times. All tests were
successful, even the ones with no possible optimal routes because of time
restrictions (in which case the results display that no routes were found). 

7. Improvement
I think the instructions were very clear and it is a very useful assignment.
I think that the time restrictions required heavy algorithm design and it
would be more interesting to instead work with more Android UI design.

