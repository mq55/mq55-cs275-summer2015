import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooSession;

public class TwitterTimelineGrader {
	/**
	 * Mariana Quinde Garcia 
	 * mq55
	 * 07/28/2015
	 * Undergoes OAuth Authorization process if use requires it and estimates the grade level required to read 30 
	 * tweets on a certain user's Twitter timeline (using Temboo Libs)
	 * @param args
	 * @throws Exception 
	 */
	public static void main (String [] args) throws Exception{
		
		// Default settings for authorization and search
		String screenName = "arneduncan";
		boolean authorizeUser = false;	
		
		// Check if user wants to authorize it's account, and what twitter account it wants to grade
		if (args.length > 0)
		{
			// Check twitter account to grade preferences
			if( args[0].contains("@")){
				screenName = args[0].substring(1);
			}
			// Check own user account authorization preferences
			else {
				if(args[0].equalsIgnoreCase("yes")){
					authorizeUser = true;
				}
			}
			
			// Check twitter account to grade preferences
			if (args.length>1){
				if( args[1].contains("@")){
					screenName = args[1].substring(1);
				}
			}
				
		}
		
		// App settings gotten from twitter
		String consumerSecret = "AhhBeFWTts4vhg33I3V92EYxLN4BcFU7MiaxpKOAUQHG6i8Iwp";
		String consumerKey = "JF48BsaQab1sQedxuWgX8mYEC";
		String tweetCount = "30";
		String accessToken = "38787529-YlYBXf49313Z3LKvdAMXX3yUWnSXrt3RlRwsq0WdF";
		String accessTokenSecret = "kfYiAsmbaBBrMfZYcEPK2xUIg4JUbbpe51NLKRMJyFWbA";
		
		
		// Instantiate TembooSession
		TembooSession session = new TembooSession("mquinde", "TweetGrader", "uUX17zYc5VsGqUXLbYUpoDAaeCMFbiS2");

		// If user wants to use their own account to search
		if(authorizeUser){
			
			System.out.println("Running with user's personal account (Need authorization)...");
			
			// The following code was obtained through Temboo's Library (referenced in README)
			// Initialize OAuth
			InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

			// Get an InputSet object for the choreo
			InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

			// Set inputs
			initializeOAuthInputs.set_ConsumerSecret(consumerSecret);
			initializeOAuthInputs.set_ConsumerKey(consumerKey);

			// Execute Choreo
			InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
			
			//End of Temboo's code
			
			// Prompt user to give access through link
			System.out.println("Please visit this link and click accept to give us access to your twitter account:\n" + initializeOAuthResults.get_AuthorizationURL());
					
			// The following code was obtained through Temboo's Library (referenced in README)
			// Get relevant info from initialize OAuth call
			String callbackId = initializeOAuthResults.get_CallbackID();
			String oauthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
			
			FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

			// Get an InputSet object for the choreo
			FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

			// Set inputs
			finalizeOAuthInputs.set_CallbackID(callbackId);
			finalizeOAuthInputs.set_OAuthTokenSecret(oauthTokenSecret);
			finalizeOAuthInputs.set_ConsumerSecret(consumerSecret);
			finalizeOAuthInputs.set_ConsumerKey(consumerKey);

			// Execute Choreo
			FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
			
			// End of Temboo's code
			
			// Get relevant info from finalize OAuth call
			accessToken = finalizeOAuthResults.get_AccessToken();
			accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
			
		}
		else{
			// Use developer's account
			System.out.println("Running with developer's personal account (No authorization needed)...");
		}
		
		System.out.println("Reading Timeline from " + screenName + "...");
		
		// The following code was obtained through Temboo's Library (referenced in README)
		// Get Timeline
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_Count(tweetCount);
		userTimelineInputs.set_AccessToken(accessToken);
		userTimelineInputs.set_AccessTokenSecret(accessTokenSecret);
		userTimelineInputs.set_ConsumerSecret(consumerSecret);
		userTimelineInputs.set_ScreenName(screenName);
		userTimelineInputs.set_ConsumerKey(consumerKey);
		
		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		
		// End of Temboo's code
		
		String results = userTimelineResults.get_Response();
		
		// Convert to a JSON object
		JsonParser tweetParser = new JsonParser();
		JsonElement tweetRoot = tweetParser.parse(results);
		JsonArray tweets = tweetRoot.getAsJsonArray();
		
		ArrayList<String> polysyllables = new ArrayList<String>();
		
		// Loop through tweets
		for (int i=0; i<tweets.size(); i++)
		{
			// Convert tweet to JSON Object
			JsonObject element = tweets.get(i).getAsJsonObject();
			
			// Get tweet text
			String text = element.get("text").getAsString();
			String[] words = text.split(" ");
			
			for (int j = 0; j<words.length; j++)
			{
				String word = words[j].toLowerCase().replace("#", "");
				if(! word.contains("http:/")){
					String wordURL = "http://api.wordnik.com:80/v4/word.json/"+ word + "/hyphenation?useCanonical=true&limit=50&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
					// Connect to the URL
					URL wUrl = new URL(wordURL);
					HttpURLConnection wordRequest = (HttpURLConnection) wUrl.openConnection();
					wordRequest.connect();
						      
					//Convert to a JSON object
					JsonParser syllablesParser = new JsonParser();
					try {
						// get number of syllables
						JsonElement syllablesRoot = syllablesParser.parse(new InputStreamReader((InputStream) wordRequest.getContent()));
						JsonArray syllables = syllablesRoot.getAsJsonArray();
					    
						// Convert word to JSON Object
						JsonObject last = (syllables.get(syllables.size()-1)).getAsJsonObject();
						    	
						// Get last syllable sequence in word
						int seq = last.get("seq").getAsInt();
						
						// add if it's polysyllabic
						if(seq>1){
							polysyllables.add(word);
						}
					}
					catch(Exception e)
					{
						
					}
				}
			}
		}
		// Print words
		int count = polysyllables.size();
		System.out.println("Polysyllabic word count in 30 tweets: " + count);
		System.out.println("Words: ");
		for (int i =0; i<count; i++){
			System.out.println((i+1) + ") " + polysyllables.get(i)); 
		}
		
		// Calculate grade
		double grade = 1.0430 * Math.sqrt(count)+ 3.1291;
		System.out.println("Grade level required to read tweets: " + grade);
	}

}
