import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooSession;
import sun.misc.BASE64Decoder;

public class DropboxMover {
	/**
	 * Mariana Quinde Garcia 
	 * mq55
	 * 07/27/2015
	 * Undergoes OAuth Authorization process, accesses user's Dropbox and moves files as specified in file __list (using Temboo Libs)
	 * @param args
	 * @throws Exception 
	 */
	public static void main (String [] args) throws Exception{
		// App settings gotten from doprbox
		String appSecret = "ssu9ygx5g1n7ndl";
		String appKey = "ymwq1bylo8857gt";
		
		// Instantiate the Choreo, using my TembooSession for this assignment
		TembooSession session = new TembooSession("mquinde", "DropboxMover", "g1I8fBPcWrLuSRsynpWogyPcsLBLZToZ");
			
		// Initialize OAuth process
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_DropboxAppSecret(appSecret);
		initializeOAuthInputs.set_DropboxAppKey(appKey);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		// Prompt user to give access through link
		System.out.println("Please visit this link and click accept to give us access to your dropbox:\n" + initializeOAuthResults.get_AuthorizationURL());
				
		// Continue and finalize authorization
		Scanner scanner = new Scanner(System.in);
		
		// Get relevant info from initialize OAuth call
		String callbackId = initializeOAuthResults.get_CallbackID();
		String tokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
		
		// Finalize OAuth process
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callbackId);
		finalizeOAuthInputs.set_DropboxAppSecret(appSecret);
		finalizeOAuthInputs.set_OAuthTokenSecret(tokenSecret);
		finalizeOAuthInputs.set_DropboxAppKey(appKey);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		// Get relevant info from finalize OAuth call
		String accessToken = finalizeOAuthResults.get_AccessToken();
		String accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		
		
		// Start Get File process to read __list file
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_AppSecret(appSecret);
		getFileInputs.set_AccessToken(accessToken);
		getFileInputs.set_AccessTokenSecret(accessTokenSecret);
		getFileInputs.set_AppKey(appKey);
		getFileInputs.set_Path("move/__list.txt");

		// Execute Choreo
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		
		// Get encoded content of file
		String responseEncoded = (getFileResults.get_Response());		
		
		// Decode the content of the file
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] decodedBytes = decoder.decodeBuffer(responseEncoded);
		String decodedString = new String(decodedBytes);
		
		// Store lines in an array
		String[] lines = decodedString.split("[\\n\\r]");
		
		// Parse from paths and to paths
		String[] fromPaths = new String[lines.length];
		String[] toPaths = new String[lines.length];
		for (int i=0; i<lines.length; i++){
			String[] paths = lines[i].split(" ");
			fromPaths[i] = "move/" + paths[0];
			toPaths[i] = paths[1] + "/" + paths[0];
		}
		
		// from every pair of paths, get file and rewrite it in the destination marked
		for(int i = 0; i<lines.length; i++){
			// Get an InputSet object for the choreo
			getFileInputs = getFileChoreo.newInputSet();

			// Set inputs
			getFileInputs.set_AppSecret(appSecret);
			getFileInputs.set_AccessToken(accessToken);
			getFileInputs.set_AccessTokenSecret(accessTokenSecret);
			getFileInputs.set_AppKey(appKey);
			getFileInputs.set_Path(fromPaths[i]);

			// Execute Choreo
			getFileResults = getFileChoreo.execute(getFileInputs);
			
			// Get encoded file content
			responseEncoded = (getFileResults.get_Response());
			
			// Decode file content
			decodedBytes = decoder.decodeBuffer(responseEncoded);
			decodedString = new String(decodedBytes);
			
			// Store lines in an array
			String[] fileLines = decodedString.split("[\\n\\r]");
			
			// Create file with destination oath (overwrite if existent)
			File file = new File(toPaths[i]);
			
			// Write each line in new file
			PrintWriter writer = new PrintWriter(file, "UTF-8");
			for (int j = 0; j<fileLines.length; j++){
				writer.println(fileLines[j]);
			}
			writer.close();

		}
		
		// Delete process for __list file
		DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

		// Get an InputSet object for the choreo
		DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

		// Set inputs
		deleteFileOrFolderInputs.set_AccessToken(accessToken);
		deleteFileOrFolderInputs.set_AppSecret(appSecret);
		deleteFileOrFolderInputs.set_AccessTokenSecret(accessTokenSecret);
		deleteFileOrFolderInputs.set_AppKey(appKey);
		deleteFileOrFolderInputs.set_Path("move/__list.txt");

		// Execute Choreo
		DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
		
		// End
		System.out.println("The files were succesfully moved.");
	}
}
